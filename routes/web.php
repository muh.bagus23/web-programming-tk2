<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('score')->name('score.')->middleware('auth')->group(function(){
    Route::get('/', [App\Http\Controllers\ScoreController::class, 'list'])->name('list');
    Route::get('/create', [App\Http\Controllers\ScoreController::class, 'create'])->name('create');
    Route::post('/create', [App\Http\Controllers\ScoreController::class, 'store'])->name('store');
    Route::get('/detail/{nim}', [App\Http\Controllers\ScoreController::class, 'detail'])->name('detail');
    Route::delete('/delete/{nim}', [App\Http\Controllers\ScoreController::class, 'delete'])->name('delete');
    Route::get('/edit/{nim}', [App\Http\Controllers\ScoreController::class, 'edit'])->name('edit');
    Route::put('/edit/{nim}', [App\Http\Controllers\ScoreController::class, 'update'])->name('update');
});