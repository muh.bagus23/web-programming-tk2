@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mb-2">
                <div class="card-header">{{ __('Nilai Quiz') }}</div>

                <div class="card-body">
                    <div>
                        <canvas id="quizChart"></canvas>
                    </div>
                </div>
            </div>

            <div class="card mb-2">
                <div class="card-header">{{ __('Nilai Tugas') }}</div>

                <div class="card-body">
                    <div>
                        <canvas id="taskChart"></canvas>
                    </div>
                </div>
            </div>

            <div class="card mb-2">
                <div class="card-header">{{ __('Nilai Absensi') }}</div>

                <div class="card-body">
                    <div>
                        <canvas id="attendanceChart"></canvas>
                    </div>
                </div>
            </div>

            <div class="card mb-2">
                <div class="card-header">{{ __('Nilai Praktek') }}</div>

                <div class="card-body">
                    <div>
                        <canvas id="practiceChart"></canvas>
                    </div>
                </div>
            </div>

            <div class="card mb-2">
                <div class="card-header">{{ __('Nilai UAS') }}</div>

                <div class="card-body">
                    <div>
                        <canvas id="finalExamsChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
    const quizChart = document.getElementById('quizChart');
    new Chart(quizChart, {
        type: 'bar',
        data: {
            labels: ['A', 'B', 'C', 'D'],
            datasets: [{
                label: 'Students',
                data: @json($quizScoreData),
                borderWidth: 1
            }]
        },
        options: {
            scales: {
            y: {
                beginAtZero: true
            }
            }
        }
    });

    const taskChart = document.getElementById('taskChart');
    new Chart(taskChart, {
        type: 'bar',
        data: {
            labels: ['A', 'B', 'C', 'D'],
            datasets: [{
                label: 'Students',
                data: @json($taskScoreData),
                borderWidth: 1
            }]
        },
        options: {
            scales: {
            y: {
                beginAtZero: true
            }
            }
        }
    });

    const attendanceChart = document.getElementById('attendanceChart');
    new Chart(attendanceChart, {
        type: 'bar',
        data: {
            labels: ['A', 'B', 'C', 'D'],
            datasets: [{
                label: 'Students',
                data: @json($attendanceScoreData),
                borderWidth: 1
            }]
        },
        options: {
            scales: {
            y: {
                beginAtZero: true
            }
            }
        }
    });

    const practiceChart = document.getElementById('practiceChart');
    new Chart(practiceChart, {
        type: 'bar',
        data: {
            labels: ['A', 'B', 'C', 'D'],
            datasets: [{
                label: 'Students',
                data: @json($practiceScoreData),
                borderWidth: 1
            }]
        },
        options: {
            scales: {
            y: {
                beginAtZero: true
            }
            }
        }
    });

    const finalExamsChart = document.getElementById('finalExamsChart');
    new Chart(finalExamsChart, {
        type: 'bar',
        data: {
            labels: ['A', 'B', 'C', 'D'],
            datasets: [{
                label: 'Students',
                data: @json($finalExamsScoreData),
                borderWidth: 1
            }]
        },
        options: {
            scales: {
            y: {
                beginAtZero: true
            }
            }
        }
    });
</script>
@endsection
