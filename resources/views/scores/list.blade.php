@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('List Student Score') }}</div>

                <div class="card-body">
                    @if(session('success'))
                        <div class="alert alert-success">{!! session('success') !!}</div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-danger">{!! session('error') !!}</div>
                    @endif

                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">NIM</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Rerata Nilai</th>
                                <th class="text-center" scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($students as $key => $student)
                            <tr>
                                <th scope="row">{!! $students->firstItem() + $key !!}</th>
                                <td>{{ $student->nim }}</td>
                                <td>{{ $student->name }}</td>
                                <td>{{ $student->score->score_average ?? 0 }}</td>
                                <td class="text-center">
                                    
                                    <form action="{{ route('score.delete',$student->nim) }}" method="POST" >
                                        @csrf
                                        @method('DELETE')
                                        <a class="ms-2 btn btn-sm btn-warning" href="{{ route('score.edit', $student->nim) }}">Edit</a>
                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure want to delete this record?')" href="{{ route('score.delete', $student->nim) }}">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $students->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
