@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Student Score') }}</div>

                <div class="card-body">
                    @if(session('success'))
                        <div class="alert alert-success">{!! session('success') !!}</div>
                    @endif

                    @if(session('error'))
                        <div class="alert alert-danger">{!! session('error') !!}</div>
                    @endif

                    <form method="POST" action="{{ route('score.update', $student->nim) }}">
                        @csrf
                        @method('PUT')

                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') ?? $student->name }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="nim" class="col-md-4 col-form-label text-md-end">{{ __('NIM') }}</label>

                            <div class="col-md-6">
                                <input id="nim" type="number" class="form-control @error('nim') is-invalid @enderror" name="nim" value="{{ old('nim') ?? $student->nim }}" required autocomplete="nim">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <hr>
                        </div>

                        <div class="row mb-3">
                            <label for="score_quiz" class="col-md-4 col-form-label text-md-end">{{ __('Nilai Quiz') }}</label>

                            <div class="col-md-6">
                                <input id="score_quiz" type="number" max="100" class="form-control @error('score_quiz') is-invalid @enderror" name="score_quiz" value="{{ old('score_quiz') ?? $student->score->score_quiz }}" required autocomplete="score_quiz">

                                @error('score_quiz')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="score_task" class="col-md-4 col-form-label text-md-end">{{ __('Nilai Tugas') }}</label>

                            <div class="col-md-6">
                                <input id="score_task" type="number" max="100" class="form-control @error('score_task') is-invalid @enderror" name="score_task" value="{{ old('score_task') ?? $student->score->score_task }}" required autocomplete="score_task">

                                @error('score_task')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="score_attendance" class="col-md-4 col-form-label text-md-end">{{ __('Nilai Absensi') }}</label>

                            <div class="col-md-6">
                                <input id="score_attendance" type="number" max="100" class="form-control @error('score_attendance') is-invalid @enderror" name="score_attendance" value="{{ old('score_attendance') ?? $student->score->score_attendance }}" required autocomplete="score_attendance">

                                @error('score_attendance')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="score_practice" class="col-md-4 col-form-label text-md-end">{{ __('Nilai Praktek') }}</label>

                            <div class="col-md-6">
                                <input id="score_practice" type="number" max="100" class="form-control @error('score_practice') is-invalid @enderror" name="score_practice" value="{{ old('score_practice') ?? $student->score->score_practice }}" required autocomplete="score_practice">

                                @error('score_practice')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="score_final_exams" class="col-md-4 col-form-label text-md-end">{{ __('Nilai UAS') }}</label>

                            <div class="col-md-6">
                                <input id="score_final_exams" type="number" max="100" class="form-control @error('score_final_exams') is-invalid @enderror" name="score_final_exams" value="{{ old('score_final_exams') ?? $student->score->score_final_exams }}" required autocomplete="score_final_exams">

                                @error('score_final_exams')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
