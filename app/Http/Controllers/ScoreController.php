<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\StudentScore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ScoreController extends Controller
{
    public function create()
    {
        return view('scores.create');
    }

    public function list(Request $request)
    {
        $students = Student::with('score')->simplePaginate(10);

        return view('scores.list', [
            'students' => $students
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => ["required"],
            "nim" => ["required"],
            "score_quiz" => ["required", "gte:0", "lte:100"],
            "score_task" => ["required", "gte:0", "lte:100"],
            "score_attendance" => ["required", "gte:0", "lte:100"],
            "score_practice" => ["required", "gte:0", "lte:100"],
            "score_final_exams" => ["required", "gte:0", "lte:100"],
        ],[
            "required" => ":attribute is required",
            "gte" => "Please fill out score between 0 - 100",
            "lte" => "Please fill out score between 0 - 100",
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        DB::beginTransaction();
        try {
            $checkStudent = Student::where(['nim' => $request->nim])->first();
            if ($checkStudent) {
                return redirect()->back()->withInput()->with('error', "Duplicate Entry - [".$request->name." - ".$request->nim."] score already submitted. <a href='".route('score.detail', $checkStudent->nim)."'>Click here to edit score</a>");
            }

            $student = Student::updateOrCreate([
                'nim' => $request->nim
            ],
            [
                'name' => $request->name
            ]);
            
            $averageScore = collect([
                $request->score_quiz,
                $request->score_task,
                $request->score_attendance,
                $request->score_practice,
                $request->score_final_exams,
            ])->average();

            $studentScores = StudentScore::updateOrCreate([
                'student_id' => $student->id
            ],
            [
                'score_quiz' => $request->score_quiz,
                'score_task' => $request->score_task,
                'score_attendance' => $request->score_attendance,
                'score_practice' => $request->score_practice,
                'score_final_exams' => $request->score_final_exams,
                'score_average' => $averageScore
            ]);

            DB::commit();

            return redirect()->back()->with('success', "Score [".$request->name." - ".$request->nim."] submitted successfully");
        } catch (\Throwable $th) {
            DB::rollBack();
            
            return redirect()->back()->withInput()->with('error', $th->getMessage());
        }        
    }

    public function delete(Request $request)
    {
        $student = Student::where('nim', $request->nim)->first();
        StudentScore::where('student_id', $student->id)->delete();
        $student->delete();
        
        return redirect()->back()->with('success', "Score [".$student->name." - ".$student->nim."] deleted successfully");
    }

    public function edit(Request $request)
    {
        $student = Student::with('score')->where('nim', $request->nim)->first();

        return view('scores.edit', [
            'student' => $student
        ]);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "name" => ["required"],
            "nim" => ["required"],
            "score_quiz" => ["required", "gte:0", "lte:100"],
            "score_task" => ["required", "gte:0", "lte:100"],
            "score_attendance" => ["required", "gte:0", "lte:100"],
            "score_practice" => ["required", "gte:0", "lte:100"],
            "score_final_exams" => ["required", "gte:0", "lte:100"],
        ],[
            "required" => ":attribute is required",
            "gte" => "Please fill out score between 0 - 100",
            "lte" => "Please fill out score between 0 - 100",
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        DB::beginTransaction();
        try {
            $student = Student::updateOrCreate([
                'nim' => $request->nim
            ],
            [
                'name' => $request->name
            ]);
            
            $averageScore = collect([
                $request->score_quiz,
                $request->score_task,
                $request->score_attendance,
                $request->score_practice,
                $request->score_final_exams,
            ])->average();

            $studentScores = StudentScore::updateOrCreate([
                'student_id' => $student->id
            ],
            [
                'score_quiz' => $request->score_quiz,
                'score_task' => $request->score_task,
                'score_attendance' => $request->score_attendance,
                'score_practice' => $request->score_practice,
                'score_final_exams' => $request->score_final_exams,
                'score_average' => $averageScore
            ]);

            DB::commit();

            return redirect()->back()->with('success', "Score [".$request->name." - ".$request->nim."] updated successfully");
        } catch (\Throwable $th) {
            DB::rollBack();
            
            return redirect()->back()->withInput()->with('error', $th->getMessage());
        }    
    }
}
