<?php

namespace App\Http\Controllers;

use App\Models\StudentScore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $dataLabels = ['A', 'B', 'C', 'D'];

        $quizChartData = StudentScore::selectRaw("
            COUNT(students.id) as num,
            (CASE 
                WHEN student_scores.score_quiz <= 65 THEN 'D' 
                WHEN student_scores.score_quiz <= 75 THEN 'C'
                WHEN student_scores.score_quiz <= 85 THEN 'B'
                WHEN student_scores.score_quiz <= 100 THEN 'A'
                ELSE 'Ungraded'
            END) as grade
        ")->join('students', 'students.id','=','student_scores.student_id')->groupBy(DB::raw("
            CASE 
                WHEN student_scores.score_quiz <= 65 THEN 'D' 
                WHEN student_scores.score_quiz <= 75 THEN 'C'
                WHEN student_scores.score_quiz <= 85 THEN 'B'
                WHEN student_scores.score_quiz <= 100 THEN 'A'
                ELSE 'Ungraded'
            END
        "))->get()->mapWithKeys(function ($item, $key) {
            return [$item['grade'] => $item['num']];
        });

        $taskChartData = StudentScore::selectRaw("
            COUNT(students.id) as num,
            (CASE 
                WHEN student_scores.score_task <= 65 THEN 'D' 
                WHEN student_scores.score_task <= 75 THEN 'C'
                WHEN student_scores.score_task <= 85 THEN 'B'
                WHEN student_scores.score_task <= 100 THEN 'A'
                ELSE 'Ungraded'
            END) as grade
        ")->join('students', 'students.id','=','student_scores.student_id')->groupBy(DB::raw("
            CASE 
                WHEN student_scores.score_task <= 65 THEN 'D' 
                WHEN student_scores.score_task <= 75 THEN 'C'
                WHEN student_scores.score_task <= 85 THEN 'B'
                WHEN student_scores.score_task <= 100 THEN 'A'
                ELSE 'Ungraded'
            END
        "))->get()->mapWithKeys(function ($item, $key) {
            return [$item['grade'] => $item['num']];
        });

        $attendanceChartData = StudentScore::selectRaw("
            COUNT(students.id) as num,
            (CASE 
                WHEN student_scores.score_attendance <= 65 THEN 'D' 
                WHEN student_scores.score_attendance <= 75 THEN 'C'
                WHEN student_scores.score_attendance <= 85 THEN 'B'
                WHEN student_scores.score_attendance <= 100 THEN 'A'
                ELSE 'Ungraded'
            END) as grade
        ")->join('students', 'students.id','=','student_scores.student_id')->groupBy(DB::raw("
            CASE 
                WHEN student_scores.score_attendance <= 65 THEN 'D' 
                WHEN student_scores.score_attendance <= 75 THEN 'C'
                WHEN student_scores.score_attendance <= 85 THEN 'B'
                WHEN student_scores.score_attendance <= 100 THEN 'A'
                ELSE 'Ungraded'
            END
        "))->get()->mapWithKeys(function ($item, $key) {
            return [$item['grade'] => $item['num']];
        });

        $practiceChartData = StudentScore::selectRaw("
            COUNT(students.id) as num,
            (CASE 
                WHEN student_scores.score_practice <= 65 THEN 'D' 
                WHEN student_scores.score_practice <= 75 THEN 'C'
                WHEN student_scores.score_practice <= 85 THEN 'B'
                WHEN student_scores.score_practice <= 100 THEN 'A'
                ELSE 'Ungraded'
            END) as grade
        ")->join('students', 'students.id','=','student_scores.student_id')->groupBy(DB::raw("
            CASE 
                WHEN student_scores.score_practice <= 65 THEN 'D' 
                WHEN student_scores.score_practice <= 75 THEN 'C'
                WHEN student_scores.score_practice <= 85 THEN 'B'
                WHEN student_scores.score_practice <= 100 THEN 'A'
                ELSE 'Ungraded'
            END
        "))->get()->mapWithKeys(function ($item, $key) {
            return [$item['grade'] => $item['num']];
        });

        $finalExamsChartData = StudentScore::selectRaw("
            COUNT(students.id) as num,
            (CASE 
                WHEN student_scores.score_final_exams <= 65 THEN 'D' 
                WHEN student_scores.score_final_exams <= 75 THEN 'C'
                WHEN student_scores.score_final_exams <= 85 THEN 'B'
                WHEN student_scores.score_final_exams <= 100 THEN 'A'
                ELSE 'Ungraded'
            END) as grade
        ")->join('students', 'students.id','=','student_scores.student_id')->groupBy(DB::raw("
            CASE 
                WHEN student_scores.score_final_exams <= 65 THEN 'D' 
                WHEN student_scores.score_final_exams <= 75 THEN 'C'
                WHEN student_scores.score_final_exams <= 85 THEN 'B'
                WHEN student_scores.score_final_exams <= 100 THEN 'A'
                ELSE 'Ungraded'
            END
        "))->get()->mapWithKeys(function ($item, $key) {
            return [$item['grade'] => $item['num']];
        });

        $quizScoreData = [];
        $taskScoreData = [];
        $attendanceScoreData = [];
        $practiceScoreData = [];
        $finalExamsScoreData = [];
        foreach ($dataLabels as $value) {
            array_push($quizScoreData, ($quizChartData[$value] ?? 0));
            array_push($taskScoreData, ($taskChartData[$value] ?? 0));
            array_push($attendanceScoreData, ($attendanceChartData[$value] ?? 0));
            array_push($practiceScoreData, ($practiceChartData[$value] ?? 0));
            array_push($finalExamsScoreData, ($finalExamsChartData[$value] ?? 0));
        }

        return view('home', [
            'quizScoreData' => $quizScoreData,
            'taskScoreData' => $taskScoreData,
            'attendanceScoreData' => $attendanceScoreData,
            'practiceScoreData' => $practiceScoreData,
            'finalExamsScoreData' => $finalExamsScoreData,
        ]);
    }
}