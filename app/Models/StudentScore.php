<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentScore extends Model
{
    use HasFactory;

    protected $fillable = [
        'score_quiz',
        'score_task',
        'score_attendance',
        'score_practice',
        'score_final_exams',
        'score_average',
        'student_id',
    ];
}
